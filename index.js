// roles : Mage, Support, Assassin, Adc, Tank;

// Character Class
class Character {
  name = "";
  role = "";

  constructor(name, role) {
    this.name = name;
    this.role = role;
  }

  armor(ar) {
    console.log(`${this.name} is ${this.role} and have ${ar} points of Armor!`);
  }
  damage(dmg) {
    console.log(
      `${this.name} is ${this.role} and have Attack Damage with ${dmg} points of Damage!`
    );
  }
}

// Mage
class Mage extends Character {
  constructor(name, role) {
    super(name, role);
  }
  damage(dmg) {
    console.log(
      `${this.name} is ${this.role} and have Magic Damage with ${dmg} points of Damage!`
    );
  }
  magicPowers() {
    console.log(`${this.name} is ${this.role} can fly!`);
  }
}

//Support
class Support extends Mage {
  heal = 0;
  constructor(name, heal) {
    super(name, "Support");
    this.heal = heal;
  }
  healing() {
    console.log(
      `${this.name} is ${this.role} and can heal ${this.heal} amount of unit!`
    );
  }
}

// Adc
class Adc extends Character {
  constructor(name) {
    super(name, "Adc");
  }
  range(rng) {
    rng > 30
      ? console.log(`${this.name} is ${this.role}. It is in far range.`)
      : console.log(`${this.name} is ${this.role}. It is in close range.`);
  }
}

// Test Cases
let karma = new Mage("Karma", "Mage");
console.log(karma);
karma.damage(10);
karma.armor(20);
karma.magicPowers();
let ashe = new Support("Ashe", 50);
console.log(ashe);
ashe.damage(10);
ashe.armor(20);
ashe.magicPowers();
ashe.healing();
let rengar = new Character("Rengar", "Assasin");
console.log(rengar);
rengar.damage(10);
rengar.armor(20);
let jax = new Adc("Jax");
console.log(jax);
jax.damage(10);
jax.armor(20);
jax.range(50);
jax.range(10);
let darius = new Character("Darius", "Tank");
console.log(darius);
darius.damage(10);
darius.armor(20);
